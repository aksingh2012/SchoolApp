var mongoose = require('mongoose');
var Schema = require('mongoose').Schema;

module.exports = {
	Users: mongoose.model('users', {
		name:String,
		phone:Number,
		email:String,
		password:String,
		pk: Number,
		salt : String,
		created_at: Date,
		address : String,
		profile_img: String,
		otp: String,
		address: String,
		extended: mongoose.Schema.Types.Mixed,
		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
	}),
	Schools: mongoose.model('schools', {
		  name: {
		    type: String,
		    require: true
		  },
		  logo: {
		    type: String,
		    "default": ""
		  },
		  address: String,
		  classes: [],
		  faculties: [],
		  description: {
		    type: String,
		    "default": ""
		  },
		  pk: Number,
		  uuid: String,
		  created_at: {
		    type: Date,
		    "default": Date.now()
		  },
		  lan: Number,
		  lat: Number,
		  pincode: Number,
		  phone: Number,
		  telephone: String,
		  active: {
		    type: Boolean,
		    "default": false
		  },
		  admin_id: String,
		  team: [],
		  facilites : [],
		  website: String
	}),
	Registration : mongoose.model('registration', {
		address: String,
		contactNo: String,
		contactPerson: String,
		email: String,
		state: String,
		district: String,
		town: String,
		packagename: String,
		amount: Number,
		username: String,
		password: String,
		schoolname:String
	}),
	Schedule : mongoose.model('schedule',{
		name: String,
		class_name: String,
		type: String,
		status: String,
		subject: String,
		date_time: Date,
		school_uuid: String,
		created_at: { type: Date, default: Date.now() }
	}),
	Task: mongoose.model('task', {
		class_name : String,
		task_detail : String,
		subject:String,
		img: String,
		school_uuid: String,
		date_time : Date,
		created_at: { type: Date, default: Date.now() }
	}),
	Gallery : mongoose.model('gallery', {
		name : String,
		path : String,
		created_at: Date,
		school_uuid: String
	}),
	Books : mongoose.model("books", {
		subject: String,
		teacher : String,
		name: String,
		class_name: String,
		cover: String,
		school_uuid: String
	}),
	Team : mongoose.model("team", {
		name: String,
		designation: String,
		contact: String,
		email: String,
		description: String,
		school_pk: Number,
		img : String
	}),
	Students: mongoose.model('students',
	{
		firstname : {type : String},
		lastname: {type : String},
		fathersname: {type : String},
		mothersname: {type : String},
		class: {type : String},
		section: {type : String},
		address: {type : String},
		profile_img:{type : String},
		dob: {type : String},
		cast: {type : String},
		rollno: {type : String},
		pk: {type : Number, default: Number(Math.random().toString().split(".")[1])},
		gender:{type : String},
		created_at:{type : Date, default: Date.now()},
		email : {type : String},
		uuid: {type:String},
		contact: Number,
		school_uuid: String,
		application_id: String
	}),
	Sliders: mongoose.model("sliders", {
		path: String,
		school_uuid: String
	}),
	Teachers: mongoose.model('teachers', {
	  first_name: String,
	  last_name: String,
	  school_uuid: String,
	  classes: [],
	  subjects: [],
	  email: String,
	  phone: String,
	  profile_img: String,
	  password: String,
	  access_level: {
	    type: Number,
	    "default": 3
	  },
	  salt: String
	}),
	Classes: mongoose.model('class', {
		name : {type:String, require: true},
		class: { type:String },
		subjects : [],
		pk: {type: Number, default: 0}
	}),
	Subjects: mongoose.model('subjects', {
		name : {type:String, require: true},
		pk: {type:Number, default: 0}
	}),
	TimeTables: mongoose.model('timetables', {
		school_pk: String,
		// teacher_id: String,
		class_pk: String,
		subjects: [
			{
				name: String,
				teacher_id: String,
				room_no: String,
				teacher_name: String
			}
		],
		day: String,
		semister: String,
		year: String,
		from: String,
		to: String
	})

	// Teacher: mongoose.model('teachers', {
	//   first_name: String,
	//   last_name: String,
	//   school_uuid: String,
	//   classes: [],
	//   subjects: [],
	//   profile_img: String,
	//   access_level: {
	//     type: Number,
	//     "default": 3
	//   }
	// })
}

var Facilities = new Schema({
	name: String,
	display_name: String,
	path: String,
	type: String
})

var Books = new Schema({
	name: String,
	cover: String,
	author: String,
	title: String,
	sub_title: String
})


var Subjects = new Schema({
	name: String,
	books: [ Books ]
})

var Classes = new Schema({
	display_name: String,
	name : { type:String, require: true },
	course: String,
	section: String,
	year: String,
	class: { type: String },
	subjects : [ Subjects ],
	pk: {type: Number, default: 0}
})

var Teachers = new Schema({
	first_name: String,
	last_name: String,
	school_uuid: String,
	classes: [ Classes ],
	subjects: [ Subjects ],
	email: String,
	phone: String,
	profile_img: String,
	password: String,
	access_level: {
		type: Number,
		"default": 3
	},
	salt: String
})

var TimeTables = new Schema({
	class: Classes,
	subjects: [	Subjects ],
	day: String,
	semister: String,
	year: String,
	from: String,
	to: String
})

var Users = new Schema({
	name:String,
	phone:Number,
	email:String,
	password:String,
	pk: Number,
	salt : String,
	profile_img: String,
	otp: String,
	address: String,
	extended: mongoose.Schema.Types.Mixed,
	timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
})

var Schedules = new Schema({
	name: String,
	class_name: String,
	type: String,
	status: String,
	subject: String,
	date_time: Date,
	school_uuid: String,
})

var Schools = new Schema({
	name: String,
	logo: {
	  type: String,
	},
	address: String,
	classes: [ Classes ],
	faculties: [ Teachers ],
	description: {
	  type: String,
	  "default": ""
	},
	pk: Number,
	uuid: String,
	lan: Number,
	lat: Number,
	pincode: Number,
	phone: Number,
	telephone: String,
	active: {
	  type: Boolean,
	  "default": false
	},
	admin: [ Users ],
	team: [ Team ],
	facilites : [ Facilities ],
	website: String,
	timetable: [ TimeTables ],
	schedules: [ Schedules ],
	timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
})



var Students = new Schema({
	firstname : {type : String},
	lastname: {type : String},
	fathersname: {type : String},
	mothersname: {type : String},
	class: [ Classes ],
	section: {type : String},
	address: {type : String},
	profile_img:{type : String},
	dob: {type : String},
	cast: {type : String},
	rollno: {type : String},
	pk: {type : Number, default: Number(Math.random().toString().split(".")[1])},
	gender:{type : String},
	email : {type : String},
	uuid: {type:String},
	contact: Number,
	school: Schools,
	application_id: String,
	timetable: [ TimeTables ],
	schedule: Schema.type.Mixed,
	timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
})


exports.Classes = mongoose.model('classes', Classes);
exports.Users = mongoose.model('users', Users);
exports.TimeTables = mongoose.model('timetables', TimeTables);
exports.Subjects = mongoose.model('subjects', Subjects);
exports.Teachers = mongoose.model('teachers', Teachers);
exports.Students = mongoose.model('students', Students);
exports.Books = mongoose.model("books", Books);
exports.Schools = mongoose.model('schools', Schools);
exports.Facilities = mongoose.model('facilities', Facilities);
exports.Schedule = mongoose.model('schedules', Schedule);