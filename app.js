var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose'); 
var index = require('./Routers/index');
var port =8080;  // port assigned 8080 
/*---------------- db connectivity----------------*/

var db ='mongodb://localhost/schoolApp';
mongoose.connect(db);

app.use(bodyParser.json())                                              // tells system that you want the json to be used 
app.use(bodyParser.urlencoded({
    extended:true                                                       //   use simple algo for parsing or use complex parsing :true
}));


app.use(index);   


/*----------- checking the port connectivity------- */
app.listen(port,function(){
    console.log("port listening at :"+port);
})